//
//  ViewController.swift
//  setupTestProject
//
//  Created by Aurélien Haie on 21/09/2020.
//  Copyright © 2020 Aurélien Haie. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .red
    }

}
