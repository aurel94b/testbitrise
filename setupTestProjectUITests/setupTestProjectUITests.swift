//
//  setupTestProjectUITests.swift
//  setupTestProjectUITests
//
//  Created by Aurélien Haie on 21/09/2020.
//  Copyright © 2020 Aurélien Haie. All rights reserved.
//

import XCTest
// swiftlint:disable all
class setupTestProjectUITests: XCTestCase {

    func setUpWithError() throws {
        print("SETUP")
        continueAfterFailure = false
    }

    func tearDownWithError() throws {
        print("TEARDOWN")
    }

    func testExample() throws {
        print("Et ca teste")
        let app = XCUIApplication()
        app.launch()
    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTOSSignpostMetric.applicationLaunch]) {
                XCUIApplication().launch()
            }
        }
    }
}
